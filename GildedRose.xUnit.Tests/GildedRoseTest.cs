using GildedRose.App;
using GildedRose.App.Data;
using GildedRose.App.Factory;
using GildedRose.Models;
using System.Collections.Generic;
using Xunit;

namespace GildedRose.xUnit.Tests
{
    public class GildedRoseTest
    {
        [Fact]
        public void CheckDefaultOutput()
        {
            List<Item> Items = DataOperations.GetList();
            var app = new Program();

            app.UpdateQuality(ref Items);


            List<Item> expectedOutput = new List<Item>()
                                          {
                                              ItemFactory.GetItem("+5 Dexterity Vest", 9, 19),
                                              ItemFactory.GetItem(Enums.agedBrieItem, 1, 1),
                                              ItemFactory.GetItem("Elixir of the Mongoose", 4, 6),
                                              ItemFactory.GetItem(Enums.sulfurasHandOfRagnarosItem, 0, 80),
                                              ItemFactory.GetItem(Enums.sulfurasHandOfRagnarosItem, -1, 80),
                                              ItemFactory.GetItem(Enums.backStageItem,14,21),
                                              ItemFactory.GetItem(Enums.backStageItem,9,50),
                                              ItemFactory.GetItem(Enums.backStageItem,4,50),
                                              ItemFactory.GetItem(Enums.conjuredManaCakeItem, 2, 4)
                                          };




            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());
            Assert.Equal(expectedOutput[2], Items[2], new ItemComparer());
            Assert.Equal(expectedOutput[3], Items[3], new ItemComparer());
            Assert.Equal(expectedOutput[4], Items[4], new ItemComparer());
            Assert.Equal(expectedOutput[5], Items[5], new ItemComparer());
            Assert.Equal(expectedOutput[6], Items[6], new ItemComparer());
            Assert.Equal(expectedOutput[7], Items[7], new ItemComparer());
            Assert.Equal(expectedOutput[8], Items[8], new ItemComparer());

        }


        //Once the sell by date has passed, Quality degrades twice as fast (Normal,Conjured )
        [Fact]
        public void DecreaseQualityTwiceIfSellInDatePassed()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem("+5 Dexterity Vest", 0, 20),
                ItemFactory.GetItem(Enums.conjuredManaCakeItem, 0, 20)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);

            List<Item> expectedOutput = new List<Item>() {
                ItemFactory.GetItem("+5 Dexterity Vest", -1, 18),
                ItemFactory.GetItem(Enums.conjuredManaCakeItem, -1, 16)
            };
            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());
        }


        //Checked Quality degrades items (Normal,Conjured )
        [Fact]
        public void QualityOfItemCanNotBeNegative()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem("+5 Dexterity Vest", 0, 0),
                ItemFactory.GetItem(Enums.conjuredManaCakeItem, 0, 0)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);

            List<Item> expectedOutput = new List<Item>() {
                ItemFactory.GetItem("+5 Dexterity Vest", -1, 0),
                ItemFactory.GetItem(Enums.conjuredManaCakeItem, -1, 0)

            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());
        }

        //Quality of an item is never more than 50 except "Sulfuras", being a legendary item
        //Checked quality increase items (Aged Brie,BackStage)
        [Fact]
        public void QualityOfanItemCanNotExceed50ExceptSulfras()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem("Aged Brie", 7, 50),
                ItemFactory.GetItem(Enums.backStageItem,14,50),
                ItemFactory.GetItem(Enums.backStageItem,9,50),
                ItemFactory.GetItem(Enums.backStageItem,4,50),
            };
            var app = new Program();
            app.UpdateQuality(ref Items);

            List<Item> expectedOutput = new List<Item>() {
                ItemFactory.GetItem("Aged Brie", 6, 50),
                 ItemFactory.GetItem(Enums.backStageItem,13,50),
                ItemFactory.GetItem(Enums.backStageItem,8,50),
                ItemFactory.GetItem(Enums.backStageItem,3,50),
            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());
            Assert.Equal(expectedOutput[2], Items[2], new ItemComparer());
            Assert.Equal(expectedOutput[3], Items[3], new ItemComparer());

        }


        //"Sulfuras", being a legendary item, never has to be sold or decreases in Quality
        [Fact]
        public void NotChangeQualityofSulfuras()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem(Enums.sulfurasHandOfRagnarosItem, 0, 80)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);

            List<Item> expectedOutput = new List<Item>() {
                ItemFactory.GetItem(Enums.sulfurasHandOfRagnarosItem, 0, 80)
            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());

        }


        // "Backstage passes", like aged brie, increases in Quality as its SellIn 
        // value approaches;  Quality increases by 2 when there are 10 days or less and 
        //by 3 when there are 5 days or less butQuality drops to 0 after the concert
        [Fact]
        public void IncreaseQuality1IfSellInMoreThan10DaysBackStage()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem(Enums.backStageItem, 11, 20)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);


            List<Item> expectedOutput = new List<Item>() {
                 ItemFactory.GetItem(Enums.backStageItem, 10, 21)
            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());

        }

        [Fact]
        public void IncreaseQuality2IfSellInLessEqualThan10DaysBackStage()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem(Enums.backStageItem, 10, 20),
                 ItemFactory.GetItem(Enums.backStageItem, 6, 20)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);


            List<Item> expectedOutput = new List<Item>() {
                 ItemFactory.GetItem(Enums.backStageItem, 9, 22),
                 ItemFactory.GetItem(Enums.backStageItem, 5, 22)
            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());

        }

        [Fact]
        public void IncreaseQuality3IfSellInLessEqualThan5DaysBackStage()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem(Enums.backStageItem, 5, 20),
                 ItemFactory.GetItem(Enums.backStageItem, 1, 20)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);


            List<Item> expectedOutput = new List<Item>() {
                 ItemFactory.GetItem(Enums.backStageItem, 4, 23),
                 ItemFactory.GetItem(Enums.backStageItem,0, 23)
            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());

        }

        [Fact]
        public void AssignQuality0IfSellInEqualLessThan0DaysBackStage()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem(Enums.backStageItem, 0, 20),
                ItemFactory.GetItem(Enums.backStageItem, -1, 20)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);


            List<Item> expectedOutput = new List<Item>() {
                 ItemFactory.GetItem(Enums.backStageItem, -1, 0),
                 ItemFactory.GetItem(Enums.backStageItem, -2, 0),
            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());

        }


        //"Conjured" items degrade in Quality twice as fast as normal items
        [Fact]
        public void QualityDecreaseTwiceOfNormalItemsConjured()
        {
            List<Item> Items = new List<Item>() {
                ItemFactory.GetItem(Enums.conjuredManaCakeItem, 5, 20),
                 ItemFactory.GetItem(Enums.conjuredManaCakeItem, 1, 20),
                ItemFactory.GetItem(Enums.conjuredManaCakeItem, 0, 20)
            };
            var app = new Program();
            app.UpdateQuality(ref Items);


            List<Item> expectedOutput = new List<Item>() {
                 ItemFactory.GetItem(Enums.conjuredManaCakeItem, 4, 18),
                 ItemFactory.GetItem(Enums.conjuredManaCakeItem, 0, 18),
                 ItemFactory.GetItem(Enums.conjuredManaCakeItem, -1, 16)
            };

            Assert.Equal(expectedOutput[0], Items[0], new ItemComparer());
            Assert.Equal(expectedOutput[1], Items[1], new ItemComparer());
            Assert.Equal(expectedOutput[2], Items[2], new ItemComparer());

        }
    }
}
