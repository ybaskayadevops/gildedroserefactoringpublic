﻿using GildedRose.App;
using System;
using System.Collections.Generic;

namespace GildedRose.xUnit.Tests
{
    public class ItemComparer : IEqualityComparer<Item>
    {
        public bool Equals(Item item1, Item item2)
        {
            bool result = false;

            if (item1.getName().Equals(item2.getName()) &&
                item1.getSellIn().Equals(item2.getSellIn()) && item1.getQuality().Equals(item2.getQuality()))
            {
                result = true;
            }

            return result;            
        }

        public int GetHashCode(Item obj)
        {
            throw new NotImplementedException();
        }
    }
}
