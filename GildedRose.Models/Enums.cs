﻿
namespace GildedRose.Models
{
    public class Enums
    {
        public const string sulfurasHandOfRagnarosItem = "Sulfuras, Hand of Ragnaros";
        public const string backStageItem = "Backstage passes to a TAFKAL80ETC concert";
        public const string agedBrieItem = "Aged Brie";
        public const string conjuredManaCakeItem = "Conjured Mana Cake";

        public static int maxQualityExceptSulfuras = 50;
        public static int minQuality = 0;
    }
}
