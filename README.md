# Gilded Rose Refactoring Kata

The idea of the exercise is to do some deliberate practice, and improve your skills at designing test cases and refactoring. 
The idea is not to re-write the code from scratch, but rather to practice designing tests, taking small steps, running the tests often, 
and incrementally improving the design.

## Required Programs

- Visual Studio 2017 or Higher
- .Net Framework 4.5 or Higher

## Installation of Required Programs

1. Download the [Visual Studio installer](https://visualstudio.microsoft.com/downloads/) for Windows.        
2. Once it is downloaded, run the installer (VSCodeUserSetup-{version}.exe). This will only take a minute.   
3. By default, VS Code is installed under C:\users\username}\AppData\Local\Programs\Microsoft VS Code.

**Note:**.NET Framework 4.5.2 or higher is required for VS Code. If you are using Windows 7, make sure you have at least .NET Framework 4.5.2 installed.

**Tip:** Setup will add Visual Studio Code to your %PATH%, so from the console you can type 'code .' to open VS Code on that folder. You will need to restart your console after the installation for the change to the %PATH% environmental variable to take effect.

## Cloning the Repository

```sh
$ git clone https://ybaskayadevops@bitbucket.org/ybaskayadevops/gildedroserefactoringpublic.git
```


## Compile and Run

**Step 1:**       
- Right click solution file and open with visual studio 2017 or higher


![alt text](https://downloaddosya.s3.amazonaws.com/ReadmeImage1.jpg)

**Step 2:**    
- You can see 3 projects under solution. Set as startup project to **GildedRose.App** project.      
- Press start button after then **GildedRose.App**  project compiles and runs.


![alt text](https://downloaddosya.s3.amazonaws.com/ReadmeImage2.jpg)

## Tests

1. Open the test explorer from visual studio. (**Test** -> **Windows** -> **Test Explorer**)  
2. Press **Run All** button to run all tests.  

![alt text](https://downloaddosya.s3.amazonaws.com/ReadmeImage3.jpg)



## Note
- I added all packages to Git so you don't have to install nuget packages manually 