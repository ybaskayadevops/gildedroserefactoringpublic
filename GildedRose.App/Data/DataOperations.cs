﻿using GildedRose.App.Factory;
using GildedRose.Models;
using System.Collections.Generic;

namespace GildedRose.App.Data
{
    public class DataOperations
    {
        public static List<Item> GetList()
        {
            List<Item> Items = new List<Item>()
                                          {
                                              ItemFactory.GetItem("+5 Dexterity Vest", 10, 20),
                                              ItemFactory.GetItem(Enums.agedBrieItem, 2, 0),
                                              ItemFactory.GetItem("Elixir of the Mongoose", 5, 7),
                                              ItemFactory.GetItem(Enums.sulfurasHandOfRagnarosItem, 0, 80),
                                              ItemFactory.GetItem(Enums.sulfurasHandOfRagnarosItem, -1, 80),
                                              ItemFactory.GetItem(Enums.backStageItem,15,20),
                                              ItemFactory.GetItem(Enums.backStageItem,10,49),
                                              ItemFactory. GetItem(Enums.backStageItem,5,49),
                                              ItemFactory.GetItem(Enums.conjuredManaCakeItem, 3, 6)
                                          };

            return Items;
        }
    }
}
