﻿using GildedRose.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.App.GildedRoseModels
{
    class BackStagePasses : Item
    {
        public BackStagePasses(string name, int sellIn, int quality) : base(name, sellIn, quality) { }
        public override Item CalculateSellIn()
        {
            return new BackStagePasses(this.Name, this.SellIn - 1, this.Quality);
        }
        public override Item CalculateQuality()
        {
            int qualityCalculated = this.Quality;
            

            if (this.SellIn+1<= 0)
                qualityCalculated = 0;
            else if (this.SellIn+1 < 6)
                qualityCalculated += 3;
            else if (this.SellIn+1 < 11)
                qualityCalculated += 2;
            else
                qualityCalculated += 1;


            if (qualityCalculated > Enums.maxQualityExceptSulfuras)
            {
                qualityCalculated = Enums.maxQualityExceptSulfuras;
            }

            return new BackStagePasses(this.Name, this.SellIn, qualityCalculated);
        }
    }
}
