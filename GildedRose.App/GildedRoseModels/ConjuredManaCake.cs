﻿using GildedRose.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.App.GildedRoseModels
{
    class ConjuredManaCake : Item
    {
        public ConjuredManaCake(string name, int sellIn, int quality) : base(name, sellIn, quality) { }

        public override Item CalculateSellIn()
        {
            return new ConjuredManaCake(this.Name, this.SellIn - 1, this.Quality);
        }

        public override Item CalculateQuality()
        {
            int qualityCalculated = this.Quality;

            if (this.SellIn > -1)
            {
                qualityCalculated -= 2;
            }
            else
            {
                qualityCalculated -= 4;
            }

            return new ConjuredManaCake(this.Name, this.SellIn, qualityCalculated);
        }
    }
}
