﻿using GildedRose.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.App.GildedRoseModels
{
    class AgedBrie : Item
    {
        public AgedBrie(string name, int sellIn, int quality) : base(name, sellIn, quality) { }

        public override Item CalculateSellIn()
        {
            return new AgedBrie(this.Name, this.SellIn - 1, this.Quality);
        }

        public override Item CalculateQuality()
        {
            int qualityCalculated = this.Quality;

            if (this.SellIn > -1)
            {
                qualityCalculated++;
            }
            else
            {
                qualityCalculated += 2;
            }


            if (qualityCalculated > Enums.maxQualityExceptSulfuras)
            {
                qualityCalculated = Enums.maxQualityExceptSulfuras;
            }

            return new AgedBrie(this.Name, this.SellIn, qualityCalculated);
        }
    }
}
