﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.App.GildedRoseModels
{
    class Normal : Item
    {
        public Normal(string name, int sellIn, int quality) : base(name, sellIn, quality) { }
        public override Item CalculateSellIn()
        {
            return new Normal(this.Name, this.SellIn - 1, this.Quality);
        }

        public override Item CalculateQuality()
        {
            if (this.Quality > 0)
                if (this.SellIn < 0)
                    return new Normal(this.Name, this.SellIn, this.Quality - 2);
                else
                    return new Normal(this.Name, this.SellIn, this.Quality - 1);

            return new Normal(this.Name, this.SellIn, this.Quality);
        }
    }
}
