﻿using GildedRose.Models;

namespace GildedRose.App
{
    public abstract class Item
    {
        protected string Name { get; set; }
        protected int SellIn { get; set; }
        protected int Quality { get; set; }

        protected Item(string name, int sellIn, int quality)
        {
            this.Name = name;
            this.SellIn = sellIn;
            this.Quality = quality;

            if (this.Quality < Enums.minQuality)
            {
                this.Quality = Enums.minQuality;
            }
        }

        public abstract Item CalculateSellIn();
        public abstract Item CalculateQuality();

        public override string ToString()
        {
            return this.Name + ", " + this.SellIn + ", " + this.Quality;
        }

        public string getName()
        {
            return this.Name;
        }

        public int getSellIn()
        {
            return this.SellIn;
        }

        public int getQuality()
        {
            return this.Quality;
        }
    }
}
