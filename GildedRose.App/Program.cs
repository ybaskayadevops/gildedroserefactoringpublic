﻿using GildedRose.App.Data;
using System;
using System.Collections.Generic;

namespace GildedRose.App
{
    public class Program
    {

        public static void Main(string[] args)
        {
            Console.WriteLine("OMGHAI!");
            List<Item> Items = DataOperations.GetList();
            var app = new Program();

            for (var i = 0; i < 31; i++)
            {
                Console.WriteLine("-------- day " + i + " --------");
                Console.WriteLine("name, sellIn, quality");
                for (var j = 0; j < Items.Count; j++)
                {
                    System.Console.WriteLine(Items[j]);
                }
                Console.WriteLine("");
                app.UpdateQuality(ref Items);
            }

            Console.ReadLine();
        }

        public void UpdateQuality(ref List<Item> Items)
        {
            List<Item> ItemsGecici = new List<Item>();
            foreach (var item in Items)
            {
                var newItem = item.CalculateSellIn().CalculateQuality();
                ItemsGecici.Add(newItem);
            }

            Items = ItemsGecici;
        }
    }
}
