﻿using GildedRose.App.GildedRoseModels;
using GildedRose.Models;



namespace GildedRose.App.Factory
{
    public static class ItemFactory
    {
        public static Item GetItem(string name, int sellIn, int quality)
        {
            switch (name)
            {
                case Enums.agedBrieItem:
                    return new AgedBrie(name, sellIn, quality);
                case Enums.backStageItem:
                    return new BackStagePasses(name, sellIn, quality);
                case Enums.conjuredManaCakeItem:
                    return new ConjuredManaCake(name, sellIn, quality);
                case Enums.sulfurasHandOfRagnarosItem:
                    return new Sulfuras(name, sellIn, quality);
                default:
                    return new Normal(name, sellIn, quality);
            }
        }
    
        
    }
}
